import React, {useEffect} from 'react'
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';

import { getCategoriesAction } from '../../store/reducers/categories';

const CategoriesSection = ({categories, getCategoriesAction}) => {

  useEffect(() => {
    getCategoriesAction()
    console.log(categories);
  }, []);


  return (
     <Grid
      container
     >
     {
       categories.map((category) =>(
        <>
          <Grid 
            container 
            xs={4}
            direction="row"
            spacing={4}
            className="category__item"
          >
            <Grid
              item
              xs={6}
              spacing={4}
            >
              <Chip 
                label={category.ctasCount}
                style={{
                  backgroundColor: `${category.color}`,
                  color: 'white'
                  }} 
              />
            </Grid>
            <Grid
              item
              xs={6}
              spacing={2}
            >
              <Typography>
                {category.name}
              </Typography>
            </Grid>
          </Grid>
        </>
       ))
     }
     </Grid>
  )
}

const mapStateToProps = (state) => {
  return { 
    categories: state.categoriesReducer,
  }
}

const mapDispatchToProps = {
  getCategoriesAction,
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesSection);
