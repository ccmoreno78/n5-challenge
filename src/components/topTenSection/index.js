import React, {useEffect} from "react";
import { connect } from "react-redux";

import {getTopTenAction} from '../../store/reducers/topTen'

const TopTenSection = ({topTen, getTopTenAction}) => {

  useEffect(() => {
    getTopTenAction()
    console.log(topTen);
  }, []);

  return(
    <h1>Top Ten Section</h1>
  )
}

const mapStateToProps = (state) => {
  return {
    topTen: state.topTenReducer,
  }
};

const mapDispatchToProps = {
  getTopTenAction,
}

export default connect(mapStateToProps, mapDispatchToProps)(TopTenSection);