import React from "react";

import TopTenSection from "../../components/topTenSection"
import CategoriesSection from "../../components/categoriesSection"

import withProvider from "../withProvider";

import '../../styles/main.scss';

const ChallengeComponent = () => {

  return(
    <>
      <CategoriesSection />
      <TopTenSection />
    </>
  )
}

export default withProvider(ChallengeComponent);