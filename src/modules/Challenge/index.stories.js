import React from 'react'
import ChallengeComponent from './index'
import { storiesOf } from '@storybook/react'


storiesOf('Challenge', module)
    .add('Component', () => {
        return (
            <ChallengeComponent />
        )
    })