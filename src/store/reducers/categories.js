const defaultState = [];

const categoriesReducer = (state=defaultState, {type, payload}) => {
  switch (type){
    case 'GET_CATEGORIES_ACTION':
      return payload;
    default:
      return state;
  }
}

export default categoriesReducer;

export const type = 'GET_CATEGORIES_ACTION';

export const getCategoriesAction = () => {
  return {
    type,
    payload: [],
  }
};
