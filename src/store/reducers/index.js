import { combineReducers } from "redux";
import categoriesReducer from './categories';
import topTenReducer from './topTen';

export default combineReducers({
  categoriesReducer,
  topTenReducer,
});
