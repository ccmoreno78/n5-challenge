const defaultState = [];

const topTenReducer = (state=defaultState, {type, payload}) => {
  switch (type){
    case 'GET_TOP_TEN_ACTION':
      return payload;
    default:
      return state;
  }
}

export default topTenReducer;

export const type = 'GET_TOP_TEN_ACTION';

export const getTopTenAction = () => {
  return {
    type,
    payload: []
  }
};