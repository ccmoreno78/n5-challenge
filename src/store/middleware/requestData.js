import axios from 'axios';

const getCategoriesUrl = 'https://private-bf0ebc-n5101.apiary-mock.com/api/spa/categories';
const getTopTenUrl = 'https://private-bf0ebc-n5101.apiary-mock.com/api/ctas/top10';

const headersConfig = {'Content-Type':'application/json'}

const requestData = store => next => action => {
  console.log('ingresa a middleware')
  if (action.type === 'GET_CATEGORIES_ACTION'){
    axios.get(getCategoriesUrl, {headers: headersConfig})
    .then((response)=>{
      const {data: {data}} = response;
      next({
        type: action.type,
        payload: data
      })
    })
    .catch((err)=>{
      console.log(err);
      next(action);
    })
  } else if(action.type === 'GET_TOP_TEN_ACTION') {
    axios.get(getTopTenUrl, {headers: headersConfig})
    .then((response)=>{
      console.log('Por aqui paso')
      const {data: {data}} = response;
      next({
        type: action.type,
        payload: data
      })
    })
    .catch((err)=>{
      console.log(err);
      next(action);
    })
  } else {
    next(action);
  }
}

export default requestData;